package Control;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import Model.Account;
import Model.Student;

public class Main {
	static Logger LOGGER = LoggerFactory.getLogger(Main.class);
	

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<Student> studentList = new ArrayList<Student>();
		Account account = new Account();
		AccountManager accm = new AccountManager();
		Validate validate = new Validate();
		
		//Thêm data account
		accm.addData();
		
		//Login
		System.out.println("Nhập account: ");
		String username = sc.nextLine();

		// Kiểm tra tài khoản tồn tại
		while (!accm.checkExistedAccount(username)) {
			System.out.println("Tài khoản không tồn tại!!!");
			System.out.println("Nhập lại tài khoản: ");
			username = sc.nextLine();
		}
		
		// Kiểm tra mật khẩu
		System.out.println("Nhập password: ");
		String password = sc.nextLine();
		while (!accm.checkPassword(username, password)) {
			System.out.println("Sai mat khau!!!");
			System.out.println("Nhap lai mat khau: ");
			password = sc.nextLine();
		}
		
		// Thay đổi name và email khi đã đúng tài khoản
		System.out.println("Nhập name: ");
		String name = sc.nextLine();
		while (validate.checkNameSize(name) == false) {
			System.out.println("Name không không vượt quá 50 kí tự: ");
			System.out.println("Nhập lại name: ");
			name = sc.nextLine();
		}
		System.out.println("Nhập email: ");		
		String email = sc.nextLine();
		while (validate.checkEmail(email) == false) {
			System.out.println("Email không đúng định dạng: ");
			System.out.println("Nhập lại email: ");
			email = sc.nextLine();
		}
		
		//thay đổi thông tin
		accm.editAccount(username, password, name, email);
		account = accm.getInfoAccount(username);
		System.out.println("Thông tin account đã được thay đổi");
		LOGGER.debug("My Debug Log  " + account);
		// Show list Student
		StudentManager stdManager = new StudentManager();
		studentList = (List<Student>) stdManager.addData();
		System.out.println("==========Danh Sách Sinh Viên===========");
		stdManager.showlistStudent(studentList);

		// Menu
		showMenu();

		String choose = null;
		while (true) {
			choose = sc.nextLine();
			boolean exit = false;
			switch (choose) {
			// filter Student By class
			case "1": 
				System.out.println("Nhập mã lớp: ");
				String sclass = sc.nextLine();
				stdManager.filterStudentsByclass(sclass);
				break;
			// sort Student By Name
			case "2":
				studentList = stdManager.sortStudentByName();
				break;
			//sort Student By Email
			case "3":
				studentList = stdManager.sortStudentByEmail();
				break;
			//sort Student By Tel
			case "4":
				studentList = stdManager.sortStudentByTel();
				break;
			// find Student By Name
			case "5":
				System.out.println("Nhập tên cần tìm : ");
				String nameStd = sc.nextLine();
				stdManager.findStudentByName(nameStd);
				break;
			//find Student By Email
			case "6":
				System.out.println("Nhập email cần tìm : ");
				String emailStd = sc.nextLine();
				stdManager.findStudentByEmail(emailStd);
				break;
			//find Student By Tel
			case "7":
				System.out.println("Nhập số điện thoại cần tìm : ");
				String tel = sc.nextLine();
				stdManager.findStudentByTel(tel);
				break;
			//add Student
			case "8":
				System.out.println("====Điền thông tin học sinh cần thêm====");
				System.out.println("Nhập ID học sinh: ");
				String id = sc.nextLine();
				System.out.println("Nhập tên : ");
				String name1 = sc.nextLine();
				while (validate.checkNameSize(name) == false) {
					System.out.println("Name không không vượt quá 50 kí tự: ");
					System.out.println("Nhập lại name: ");
					name = sc.nextLine();
				}
				System.out.println("Nhập mail : ");
				String mail = sc.nextLine();
				while (validate.checkEmail(mail) == false) {
					System.out.println("Email không đúng định dạng: ");
					System.out.println("Nhập lại email: ");
					emailStd = sc.nextLine();
				}
				System.out.println("Nhập lớp : ");
				String class1 = sc.nextLine();
				System.out.println("Nhập số điện thoại: ");
				String teln = sc.nextLine();
				System.out.println("ghi chú: ");
				String note = sc.nextLine();
				stdManager.addStudent(id, name1, mail, class1, teln, note);
				break;
			//edit Student ById
			case "9":
				System.out.println("Nhập ID sinh viên cần sửa: ");
				String idEdit = sc.nextLine();
				stdManager.editStudentById(idEdit);
				break;
			//delete Student ById
			case "10":
				System.out.println("Nhập ID cần tìm: ");
				String idDelete = sc.nextLine();
				stdManager.deleteStudentById(idDelete);
				break;
				
			// exit
			case "0":
				System.out.println("exited!");
				exit = true;
				break;
			default:
				System.out.println("invalid! please choose action in below menu:");
				break;
			}
			if (exit) {
				break;
			}
			// show menu
			System.out.println("==========Danh Sách Sinh Viên===========");
			stdManager.showlistStudent(studentList);
			showMenu();
		}
	}

	private static void showMenu() {
		System.out.println("-----------Menu------------");
		System.out.println("1. Lọc sinh viên theo lớp. ");
		System.out.println("2. Sắp xếp sinh viên theo tên. ");
		System.out.println("3. Sắp xếp sinh viên theo email. ");
		System.out.println("4. Sắp xếp sinh viên theo số điện thoại. ");
		System.out.println("5. Tìm sinh viên theo tên. ");
		System.out.println("6. Tìm sinh viên theo email");
		System.out.println("7. Tìm sinh viên theo số điện thoại");
		System.out.println("8. Thêm sinh viên. ");
		System.out.println("9. Sửa thông tin sinh viên theo ID");
		System.out.println("10. Xóa Sinh viên theo ID");
		System.out.println("0. exit.");
		System.out.println("---------------------------");
		System.out.print("Please choose: ");

	}
}
