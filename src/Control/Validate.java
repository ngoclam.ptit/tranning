package Control;
import java.util.regex.Pattern;

public class Validate {
	public boolean checkEmail(String email) {
        String EMAIL_PATTERN = 
            "^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";          
        return Pattern.matches(EMAIL_PATTERN, email);

    }
	
	public boolean checkNameSize(String name) {
		int size = name.length();
		if (size > 50) {
			return false;
		}
		return true;
	}
}
