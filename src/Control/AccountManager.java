package Control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


import Model.Account;
import Model.Student;

public class AccountManager {
	Scanner sc = new Scanner(System.in);
	Map<String , Account> account = new HashMap<>();
	
	
	public void addData() {
		Account ac1 =new Account();
		ac1.setUsername("admin");
		ac1.setPassword("abc");
		ac1.setName("Nguyen Van A");
		ac1.setEmail("nguyenvana@gmail.com");	
		account.put(ac1.getUsername(), ac1);
		
		Account ac2 =new Account();
		ac2.setUsername("admin1");
		ac2.setPassword("password");
		ac2.setName("Nguyen Van B");
		ac2.setEmail("nguyenvanb@gmail.com");	
		account.put(ac2.getUsername(), ac2);

		
		Account ac3 =new Account();
		ac3.setUsername("admin3");
		ac3.setPassword("password");
		ac3.setName("Nguyen Van C");
		ac3.setEmail("nguyenvanb@gmail.com");	
		account.put(ac3.getUsername(), ac3);
		
	}
	
	/**
	 *
	 * @param username
	 * 
	 * @return Account
	 */
	 public Account getInfoAccount(String username) {
		 return account.get(username);
				
	 }
	 
	 /**
	 *
	 * @param username
	 * 
	 * @return true, false
	 */
	 public boolean checkExistedAccount(String username){
		 if(account.containsKey(username)) {
			 return true;
		 } else {
			 return false;
		 }		 
		 
	 }

	 /**
	 *
	 * @param username, password
	 * 
	 * @return true, false
	 */
	public boolean checkPassword(String username,  String password) {
		Account acc = account.get(username);
		if(acc.getPassword().equals(password)) {	
			return true;
		} else {
			return false;
		}
		
		// TODO Auto-generated method stub
		
	}

	/**
	 *
	 * @param username, password, name, email
	 * 
	 * @return 
	 */
	public void editAccount(String username, String password, String name, String email) {
		Account acc = new Account();
		acc.setUsername(username);
		acc.setPassword(password);
		acc.setName(name);
		acc.setEmail(email);
		account.replace(username, acc);		
	}


	
//	public String inputUsername() {
//		return sc.nextLine();
//	}
//	
//	public String inputPassword() {
//		return sc.nextLine();
//	}
//	public String inputName() {
//		return sc.nextLine();
//	}
//	public String inputMail() {
//		return sc.nextLine();
//	}
}
