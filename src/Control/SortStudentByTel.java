package Control;

import java.util.Comparator;

import Model.Student;

public class SortStudentByTel implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        return student1.getTel().compareTo(student2.getTel());
    }
}
