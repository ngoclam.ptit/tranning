package Control;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import Model.Student;

public class StudentManager {
//	private List<Student> studentList;
//	private StudentDAO studentDAO;
//	
//	public StudentManager() {
//		studentDAO = new StudentDAO();
//		studentList = studentDAO.read();		
//	}
//	
//	 public void show() {
//	        for (Student student : studentList) {
//	            System.out.format("%5d | ", student.getId());
//	            System.out.format("%20s | ", student.getName());
//	            System.out.format("%5d | ", student.getTel());
//	            System.out.format("%20s | ", student.getEmail());
//	            System.out.format("%20s | ", student.getSclass());
//	            System.out.format("%20s | ", student.getNote());
//	        }
//	 }	 
		List<Student> studentList = new ArrayList<Student>();
		Scanner sc = new Scanner(System.in);
		Validate validate = new Validate();
		
		/**
		 *
		 * @param 
		 * 
		 * @return List<Student>
		 */
		public List<Student> addData() {
			Student student =new Student();
			student.setId("11");
			student.setName("Nguyen Van A");
			student.setEmail("nguyenvana@gmail.com");
			student.setTel("01234511212");
			student.setSclass("D13");
			student.setNote("abc");
			studentList.add(student);
			
			Student student1 =new Student();
			student1.setId("3");
			student1.setName("Nguyen Van c");
			student1.setEmail("nguyenvand@gmail.com");
			student1.setTel("01234511214");
			student1.setSclass("D13");
			student1.setNote("abc");
			studentList.add(student1);
			
			Student student3 =new Student();
			student3.setId("5");
			student3.setName("Nguyen Van E");
			student3.setEmail("nguyenvanE@gmail.com");
			student3.setTel("01234511216");
			student3.setSclass("D14");
			student3.setNote("abc");
			studentList.add(student3);
			
			Student student4 =new Student();
			student4.setId("2");
			student4.setName("Nguyen Van B");
			student4.setEmail("nguyenvanb@gmail.com");
			student4.setTel("01234511213");
			student4.setSclass("D13");
			student4.setNote("abc");
			studentList.add(student4);
			
			
			
			Student student2 =new Student();
			student2.setId("4");
			student2.setName("Nguyen Van D");
			student2.setEmail("nguyenvanD@gmail.com");
			student2.setTel("01234511215");
			student2.setSclass("D14");
			student2.setNote("abc");
			studentList.add(student2);
			
			return studentList;
			
		}

		/**
		 *
		 * @param sclass
		 * 
		 * @return 
		 */
		public void filterStudentsByclass(String sclass) {
			int size = studentList.size();
			List<Student> listByClass = new ArrayList<>();
			for (int i = 0; i < size; i++) {
				Student std = studentList.get(i);
				if (std.getSclass().equals(sclass)) {
					listByClass.add(std);	
				}
			}
			int sz = listByClass.size();
			System.out.println("Lớp " + sclass + " có " + sz + " thành viên ");
			showlistStudent(listByClass);
			
		}
		
		/**
		 *
		 * @param 
		 * 
		 * @return studentList
		 */
		public List<Student> sortStudentByName() {
			Collections.sort(studentList, new SortStudentByName());
			return studentList;				
		}
		
		/**
		 *
		 * @param 
		 * 
		 * @return studentList
		 */
		public List<Student> sortStudentByEmail() {
			Collections.sort(studentList, new SortStudentByEmail());
			return studentList;	
	
		}
		
		/**
		 *
		 * @param 
		 * 
		 * @return studentList
		 */
		public List<Student> sortStudentByTel() {
			Collections.sort(studentList, new SortStudentByTel());
			return studentList;
		}

		/**
		 *
		 * @param nameStd
		 * 
		 * @return 
		 */
		public void findStudentByName(String nameStd) {
			boolean isExisted = false;
			List<Student> stdL = new ArrayList<>();
	        int size = studentList.size();
	        for (int i = 0; i < size; i++) {
	        	Student std = studentList.get(i);
	            if (std.getName().equals(nameStd)) {
	                isExisted = true;
	                stdL.add(studentList.get(i));
	                break;
	            }else {
	            	isExisted = false;
				}
	        }
	        if (isExisted == false) {
	            System.out.println("Không tồn tại!!");
	        } else {
	        	showlistStudent(stdL);
	        }
		}

		
		public void findStudentByEmail(String emailStd) {
			while (validate.checkEmail(emailStd) == false) {
				System.out.println("Email không đúng định dạng: ");
				System.out.println("Nhập lại email: ");
				emailStd = sc.nextLine();
			}
			boolean isExisted = false;
			List<Student> stdL = new ArrayList<>();
	        int size = studentList.size();
	        for (int i = 0; i < size; i++) {
	        	Student std = studentList.get(i);
	            if (std.getEmail().equals(emailStd)) {
	                isExisted = true;
	                stdL.add(studentList.get(i));
	                break;
	            }else {
	            	isExisted = false;
				}
	        }
	        if (isExisted == false) {
	            System.out.println("Không tồn tại!!");
	        } else {
	        	showlistStudent(stdL);
	        }
			
		}

		public void findStudentByTel(String telStd) {
			boolean isExisted = false;
			List<Student> stdL = new ArrayList<>();
	        int size = studentList.size();
	        for (int i = 0; i < size; i++) {
	        	Student std = studentList.get(i);
	            if (std.getTel().equals(telStd)) {
	                isExisted = true;
	                stdL.add(studentList.get(i));
	                break;
	            }else {
	            	isExisted = false;
				}
	        }
	        if (isExisted == false) {
	            System.out.println("Không tồn tại!!");
	        } else {
	        	showlistStudent(stdL);
	        }
			
		}

		public void addStudent(String id, String name1, String mail, String class1, String teln, String note) {
			Student std = new Student();
			std.setId(id);
			std.setName(name1);
			std.setEmail(mail);
			std.setTel(teln);
			std.setSclass(class1);
			std.setNote(note);
			
			studentList.add(std);
			showlistStudent(studentList);
			System.out.println("Đã thêm thành công!!!");
			
		}

		public void editStudentById(String id1) {
			boolean isExisted = false;
	        int size = studentList.size();
	        for (int i = 0; i < size; i++) {
	            if (studentList.get(i).getId().equals(id1)) {
	            	Student student = studentList.get(i);
	                isExisted = true;
	                System.out.println("Nhập tên cần sửa: ");
	                String ten = sc.nextLine();
	                if (ten == null || ten.isEmpty()) {
	                	ten = student.getName();
	                } else {
	                	while (validate.checkNameSize(ten) == false) {
	            			System.out.println("Name không không vượt quá 50 kí tự: ");
	            			System.out.println("Nhập lại name: ");
	            			ten = sc.nextLine();
	            		}
	                	student.setName(ten);
	                }
	                System.out.println("Nhập mail cần sửa : ");
	                String mail = sc.nextLine();
	                if (mail == null || ten.isEmpty()) {
	                	mail = student.getName();
	                } else {
	                	while (validate.checkEmail(mail) == false) {
	        				System.out.println("Email không đúng định dạng: ");
	        				System.out.println("Nhập lại email: ");
	        				mail = sc.nextLine();
	        			}
	                	student.setEmail(mail);
	                }
	                System.out.println("Nhập số điện thoại cần sửa : ");
	                String tel = sc.nextLine();
	                if (tel == null || tel.isEmpty()) {
	                	tel = student.getTel();
	                } else {
	                	student.setTel(tel);
	                }
	                System.out.println("Nhập lớp cần sửa: ");
	                String lop = sc.nextLine();
	                if (lop == null || lop.isEmpty()) {
	                	lop = student.getSclass();
	                } else {
	                	student.setSclass(lop);
	                }
	                
	                System.out.println("Nhập note cần sửa: ");
	                String note = sc.nextLine();
	                if (note == null || note.isEmpty()) {
	                	note = student.getNote();
	                } else {
	                	student.setSclass(note);
	                }
	                break;
	            }
	        }
	        if (isExisted == false) {
	            System.out.println(" Not existed!!!");
	        }
		}

		public void deleteStudentById(String idDelete) {
			boolean isExisted = false;
	        int size = studentList.size();
	        for (int i = 0; i < size; i++) {
	            if (studentList.get(i).getId().equals(idDelete)) {
	                isExisted = true;
	                System.out.println(" Đã xóa sinh viên có ID = " + idDelete );
	                studentList.remove(i);
	                
	                break;
	            }
	        }
	        if (isExisted == false) {
	            System.out.println(" Not existed!!!");
	        }
			
		}


		
		public void showlistStudent(List<Student> listStudent) {
			int size = listStudent.size();
			for (int i = 0; i < size; i++) {
				System.out.println(listStudent.get(i).showStudent());
			}
		}


	
}
