package Control;

import java.util.Comparator;

import Model.Student;

public class SortStudentByEmail implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        return student1.getEmail().compareTo(student2.getEmail());
    }
}